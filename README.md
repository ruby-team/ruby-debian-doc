# About

ruby-debian-doc is an offline alternative to Ruby documentation websites such
as http://rubydoc.info/

Although rubydoc.info provides a fantastic service to the Ruby community,
sometimes we don't have a good internet connection. This package allows you to
generate and view documentation from Debian Ruby packages installed on your
local Debian system.

# Usage

$ ruby-debian-doc <package>

That's it.

After building the documentation, ruby-debian-doc will spawn a web browser to
view the documentation by calling `xdg-open`.

# Copyright

© 2011 Antonio Terceiro <terceiro@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
