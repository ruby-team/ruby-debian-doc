Feature: build documentation

  Scenario: cucumber
    When I run "ruby-debian-doc ruby-term-ansicolor"
    Then the documentation for "ruby-term-ansicolor" should be built
    And the documentation for "ruby-term-ansicolor" should be opened in a browser

  Scenario: non-installed package
    # assuming ruby-foobarbaz does not exist!
    When I run "ruby-debian-doc ruby-foobarbaz"
    Then the error output should be empty
    And the output should contain "ruby-foobarbaz is not installed"

  Scenario: non-Ruby package
    When I run "ruby-debian-doc dpkg"
    Then the error output should be empty
    And the output should contain "dpkg does not look like Ruby software"
