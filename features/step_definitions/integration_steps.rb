root = File.join(File.dirname(__FILE__), '..', '..')
fakebindir = File.join(root, 'features', 'bin')
bindir = File.join(root, 'bin')
ENV['RUBYLIB'] = File.join(root, 'lib')
ENV['PATH'] = [fakebindir, bindir, ENV['PATH']].join(':')
tmpdir = File.join(root, 'features', 'tmp')
basedir = File.join(tmpdir, 'base')
ENV['RUBY_DEBIAN_DOC_BASE'] = basedir

require 'fileutils'

Before do
  FileUtils.mkdir_p(tmpdir)
end

After do
  FileUtils.rm_rf(tmpdir)
end

require 'rspec'

When /^I run "([^"]*)"$/ do |command|
  system("#{command} > #{tmpdir}/stdout 2> #{tmpdir}/stderr")
  @stdout = File.read("#{tmpdir}/stdout")
  @stderr = File.read("#{tmpdir}/stderr")
end

Then /^the documentation for "([^"]*)" should be built$/ do |package|
  index = File.join(basedir, package, 'index.html')
  File.exists?(index) || raise("Documentation for #{package} (#{index}) not found!")
end

Then /^the documentation for "([^"]*)" should be opened in a browser$/ do |package|
  opened_files = File.readlines(File.join(tmpdir, 'xdg-open.log')).map do |f|
    File.expand_path(f.strip)
  end
  opened_files.should include(File.expand_path(File.join(basedir, package, 'index.html')))
end

Then /^the output should contain "([^"]*)"$/ do |text|
  @stdout.should =~ Regexp.new(text)
end

Then /^the error output should be empty$/ do
  @stderr.should be_empty
end

