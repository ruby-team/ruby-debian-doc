require 'debian-doc/writer'
require 'debian-doc/package'

module Debian

  module Doc

    module CLI

      class << self

        def run(*args)
          writer = Debian::Doc::Writer.new
          package = Debian::Doc::Package.new(args.first)
          path = writer.write(package)
          index = File.join(path, 'index.html')
          Kernel.system("xdg-open", index)
        rescue Debian::Doc::Package::NotFound
          puts "#{args.first} is not installed"
        rescue Debian::Doc::Writer::NotRubySoftware
          puts "#{args.first} does not look like Ruby software"
        end

      end

    end

  end

end
