require 'tmpdir'
require 'zlib'

module Debian

  module Doc

    # The Package class represents a Debian package. It contains the list of
    # files that should be parsed with YARD in order to generate the
    # documentation for a given package.
    class Package

      class NotFound < Exception; end

      class << self

        def register(instance)
          @instances ||= []
          @instances << instance
        end

        def cleanup
          @instances.each do |package|
            FileUtils.rm_rf(package.tmpdir)
          end
        end

      end

      attr_reader :name

      def initialize(name)
        @name = name
        self.class.register(self)
        if version.nil?
          raise Debian::Doc::Package::NotFound.new
        end
      end

      def code
        files.select do |filename|
          File.basename(File.dirname(filename)) == 'vendor_ruby'
        end
      end

      def readmes
        files.select do |filename|
          File.basename(filename) =~ /^README/ && File.basename(filename) !~ /README.Debian/
        end.map do |readme|
          if readme =~ /\.gz$/
            uncompressed = File.join(tmpdir, File.basename(readme.gsub(/\.gz$/, '')))
            Zlib::GzipReader.open(readme) do |input|
              File.open(uncompressed, 'w') do |output|
                output.write(input.read)
              end
            end
            uncompressed
          else
            readme
          end
        end
      end

      def tmpdir
        @tmpdir ||= Dir.mktmpdir
      end

      def version
        @version ||= `dpkg -s #{name} 2>/dev/null | grep Version: `.strip.split(/:\s*/).last
      end

      protected

        def files
          @files ||= `dpkg -L #{name}`.split
        end

    end

  end

end

at_exit do
  Debian::Doc::Package.cleanup
end
