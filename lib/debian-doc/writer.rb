require 'fileutils'
module Debian

  module Doc

    class Writer

      class NotRubySoftware < Exception; end

      def write(package)
        raise NotRubySoftware if package.code.empty?

        output_directory = File.join(base_output_dir, package.name)

        rebuild = true
        version_file = File.join(output_directory, 'VERSION')

        if File.exists?(version_file)
          version = File.read(version_file).strip
          rebuild = (version != package.version)
        else
          rebuild = true
        end

        if rebuild
          FileUtils.rm_rf(output_directory)
          FileUtils.mkdir_p(File.dirname(output_directory))
          backend.process(output_directory, package.readmes.first, package.code)
          File.open(version_file, 'w') do |f|
            f.puts(package.version)
          end
        end
        output_directory
      end

      def base_output_dir
        ENV['RUBY_DEBIAN_DOC_BASE'] || File.join(ENV['HOME'], '.cache', 'ruby-debian-doc')
      end

      def backend
        @backend ||= Debian::Doc::Writer::YARD.new
      end

      class YARD
        def process(output_directory, readme, source)
          db = File.join(output_directory, '..', '.yardoc')
          argv = ['yardoc', '--db', db]
          if readme
            argv << '-r' << readme
          end
          argv << '-o' << output_directory
          source.each do |entry|
            argv << entry
          end
          system(*argv)
          output_directory
        end
      end

    end

  end

end
