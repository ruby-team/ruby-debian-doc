# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "debian-doc/version"

Gem::Specification.new do |s|
  s.name        = "ruby-debian-doc"
  s.version     = Debian::Doc::VERSION
  s.authors     = ["Antonio Terceiro"]
  s.email       = ["terceiro@debian.org"]
  s.homepage    = "http://wiki.debian.org/Ruby"
  s.summary     = %q{local Ruby documentation viewer for Debian}
  s.description = %q{This package generates documentation from Debian Ruby packages installed on your system and allows you to browse them}

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]

  s.add_development_dependency "rspec"
  s.add_development_dependency "cucumber"

  s.add_runtime_dependency "yard"
end
