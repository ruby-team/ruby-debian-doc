require 'rspec'
require 'debian-doc/package'
describe Debian::Doc::Package do

  before do
    @package = Debian::Doc::Package.new('ruby-rspec-core')
  end

  it 'should list entries in /usr/lib/ruby/vendor_ruby' do
    @package.code.should include('/usr/lib/ruby/vendor_ruby/rspec')
  end

  it 'should list README' do
    @package.stub(:files).and_return(["/usr/share/doc/#{@package.name}/README"])
    @package.readmes.should include("/usr/share/doc/#{@package.name}/README")
  end

  it 'should uncompress compressed READMES' do
    @package.stub(:files).and_return(["#{File.dirname(__FILE__)}/../sample/README.gz"])
    @package.readmes.should include("#{@package.tmpdir}/README")
  end

  %w[ README README.md README.rdoc ].each do |valid_readme|
    it "should list #{valid_readme}" do
      @package.stub(:files).and_return([valid_readme])
      @package.readmes.should include(valid_readme)
    end
  end

  %w[ README.Debian README.Debian-sources ].each do |debian_readme|
    it "skips #{debian_readme} files" do
      readme_file = '/path/to/' + debian_readme
      @package.stub(:files) { [readme_file ] }
      @package.readmes.should_not include(readme_file)
    end
  end

  it 'knows its version' do
    @package.version.should =~ /[0-9.-]/
    @package.version.should_not =~ /\n/
  end

  it 'raises an error if the package does not exist' do
    (lambda { Debian::Doc::Package.new('unexisting-package') }).should raise_exception(Debian::Doc::Package::NotFound)
  end

end
