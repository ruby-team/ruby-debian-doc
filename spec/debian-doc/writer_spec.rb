require 'rspec'
require 'debian-doc/writer'

describe Debian::Doc::Writer do

  class TestBackend
    attr_reader :times_called, :output, :readme, :input
    def process(output, readme, input)
      @times_called ||= 0
      @times_called += 1
      @output = output
      @readme = readme
      @input = input
      FileUtils.mkdir(output)
    end
    def self.instance
      @instance ||= self.new
    end
    def self.base
      File.join(File.dirname(__FILE__), '..', 'tmp')
    end
    def self.reset
      @instance = nil
      FileUtils.rm_rf(base)
    end
  end

  before(:all) do
    @package = Debian::Doc::Package.new('ruby-rspec-core')
    @writer = Debian::Doc::Writer.new
    def @writer.backend
      TestBackend.instance
    end
    def @writer.base_output_dir
      TestBackend.base
    end
  end

  after(:each) do
    TestBackend.reset
  end

  it 'creates output directory' do
    File.exists?(@writer.write(@package)).should be_true
  end

  it 'does not write docs for the same version twice' do
    2.times { @writer.write(@package) }
    TestBackend.instance.times_called.should == 1
  end

  it 'rebuilds for new versions' do
    @writer.write(@package)
    @package.stub(:version).and_return('new-version')
    @writer.write(@package)
    TestBackend.instance.times_called.should == 2
  end

  it 'does not accept non-Ruby packages' do
    @package.stub(:files) { [] }
    (lambda { @writer.write(@package) }).should raise_exception(Debian::Doc::Writer::NotRubySoftware)
  end

end
